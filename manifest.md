# [manifeste]

À partir des échanges, sources, réflexions personnelles des mois derniers,

[je](https://twitter.com/jibe_jeybee), me lance à écrire un manifeste ouvrant la porte à une coopérative d'activité de recherche et d'entreprenariat (independant scholarship co-op).

Ce manifeste est mis à disposition de qui voudra y contribuer (pull request) ou poursuivre de son côté (fork). Venez donc ajouter votre nom et vos sensibilités au projet ! :)

## Réflexions

### Des savoirs aux questions à la résolution

De la cognition nait la capacité à réfléchir rationnellement, à aquérir les savoirs transmis par l'entourage (enseignement, sous toutes ses formes)

et de ces savoirs naissent les questions, qui en viennent à buter sur les limites des modèles employés (questionnement, sous toutes leurs formes)

la recherche scientifique résoud cette tension par l'engagement dans le processus chaotique et fort peu plannifiable de l'emploi de combinaisons de savoirs et techniques encore rarement combinées pour obtenir des éléments construisant une ou des réponses (recherche scientifique).

### Quelle place pour les individus dans cette tension ?

La capacité de réflexion de chacun·e est très sensible à la pyramide de Maslow.

Les problématiques économiques se rappellent à chaque repas, alors que l'engagement exigé par ces questions sur sables mouvants empêche une conversion de l'individu en force de travail, en capital humain.

Comment résoudre le resserrement que forme la pointe de la pyramide de Maslow ? Comment ouvrir des espaces de liberté basés sur une stabilité économique ?

Oser ouvrir une faille dans le marché de l'offre de main d'œuvre contre pouvoir d'achat amène à construire une (ou des) gouvernances entre ceux·celles qui auront un pied dans cette brèche et mettront en commun moyens et risques,

tout autant que des modèles d'interaction avec les autres formes de structuration de l'activité. 

### Des critères comme boussole

Au sein d'un groupe, les jugements successifs amplifient des projets et atténuent les dynamiques. Les critères sur lesquels se basent ces jugements sont à choisir avec soin.

Pourquoi pas viser le bien-être de l'individu et du groupe ? (exemple : primauté de la motivation intrinsèque [illustration](https://twitter.com/zehavoc/status/961142902486458368))

Et inclure l'équilibre et la régénération du cadre comme un·e associé·e ?



Enfin, la gouvernance elle-même apportera des critères, comme celui de la pérennité de l'activité (but propre d'une entreprise coopérative).



## Fondamentaux

### Décorrelation recherche et économie

_Les activités de recherche scientifique sont par essence décorrellées de l'activité économique de par leur recherche de ce que l'on ne maîtrise pas encore tout en faisant usage de méthodes rares ou peu éprouvées._

Cette constatation nous amène à vouloir construire un cadre favorable au développement d'activités de recherche scientifique, dont la survie ne dépende pas du marché libéral.

### Corrélation compétences et économie

_Toutefois, les méthodes, instruments et productions de la recherche scientifique participent de l'activité économique globale et peuvent avoir des accroches particulièrement fortes._

Une structuration d'un cadre pour les activités de recherche scientifique peut donc se faire en incluant l'interaction entre les compétences et expertises dévouées à la recherche mises au service d'activités dont le but n'est pas la recherche.

### Décorrellation avec les échanges monétaires

_La décorrelation des buts de recherche scientifique et des buts productivistes a pour conséquence de ne pas faire reposer cette activité sur la monnaie. De la même manière que les compétences peuvent être mises au service d'une activité productiviste, les échanges monétaires peuvent être présents et mis au service pour fluidifier des échanges._

La non-dépendance aux échanges monnétaires ouvre des portes comme elle provoque des tensions avec le mode d'organisation majoritaire en occident. Méthodes originales et choix tels que la frugalité sont en opposition avec le modèle productiviste dominant mais peuvent être reformalisés dans le modèle libéral comme une optimisation d'un jeu de paramètres spécifique. Il s'agit donc de frictions mais pas d'opposition.

### Corrélation avec l'enseignement

_La recherche scientifique (qu'est-ce donc que la recherche scientifique, d'ailleurs ?) est une extension du processus d'acquisition de savoirs et de questionnements. La recherche scientifique est le bras équipé des questionnements : puisque la question existe et les moyens sont éventuellement là, à la sérendipité près, alors toute activité amenant à proposer des pistes et éventuellement des pistes pouvant servir d'outil pour creuser sur d'autres questions ; participe de la recherche scientifique._

La recherche scientifique est en soni une extension de l'enseignement. La diffusion de savoirs et l'accompagnement de l'aquisition de ces savoirs peut être comparé aux semis dans une activité agricole, où les moissons sont l'acivité saillante.

### Tous chercheu·se·r·s


_Les activités de recherche scientifique coûteuses en équipements et de niveaux de complexité élevé demandent des investissements colossaux. Cependant, ce type d'activité est une partie de ce qui relève de la recherche scientifique et une infinité de tâches, de complexité importante mais accessible à tout un chacun (la plus complexe étant la décision) participent de la recherche scientifique._

La recherche contributive invitant tout un chacun à participer est un des pilliers du développement de l'activité de recherche scientifique.

### Subjectivité statistique

_. Motivation intrinsèque plutôt que gouvernance par le haut ._

_. Et puis surtout, limitation ._


D'où le choix de surfer sur les les subjectivités et construire une objectivité collective tout en faisant vivre les subjectivités.





## Tous chercheu·se·r·s !

_Les sciences participatives, contributives, etc_

en pratique



# Gouvernance




## 1 personne = 1 voix et proxy

_Au cœur même des coopératives, une personne a une voix._

_Chaque coopérateur·rice devrait être consultée pour toute décision. En pratique, il y a trop de questions pour un individu et les individus peuvent identifier les votants qui ont les mêmes sensibilités et votant de la même manière sur telle décision. Il est alors pertinent de choisir un mode de vote par proxy._


En pratique, un mécanisme de vote comme Loomio pour permettre le vote sur chaque décision, et un mécanisme de proxy comme LiquidFeedback.



## Double qualité

Enseignant et chercheur ?


