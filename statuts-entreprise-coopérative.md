# Statuts d'entreprise coopérative

## Préambule

Ces statuts contiendront beaucoup de naïvetés. Les mécanismes permettant collectivement de les faire progresser 

## Article 1

Ce collectif est une coopérative et a pour vocation de porter les activités de recherche scientifique de ses membres et contribuer à la pérennisation d'activités de recherche en son sein et au-dehors.

## Article 2

Les modes de gouvernance visent à donner le plus de place possible à la coopération, à la discussion, au conscensus et à la progression incrémentale vers les objectifs du collectif.

## Article 3

Les critères sur lesquels développer la coopérative sont :
- la pérennité de l'activité au-delà des compromis
- l'indépendance
- la reproductibilité des productions scientifiques
- la solidarité, en particuliers avec les minorités invisibilisées

## Article 4

Chacun·e des membres est consulté pour chacune des décisions à prendre. La délégation de vote pour des groupes de consultations a vocation à fluidifier les consultations et peut être modifiée à tout moment.

Les Assemblées Générales sont une consultation de grande ampleur.
