# My own coop

## Une exploration

Vous trouverez ici l'exploration du cadre des coopératives pour l'exercice d'activités de recherche scientifique.

Nombre de questions relevant de la gouvernance sont autonomes et leur concrétisation relève de l'engagement d'individus autour d'un ou de projets communs.

À ces questions s'ajoutent les enjeux spécifiques aux activités de recherche scientifique et en particuliers l'enjeu de l'équilibre entre activités basés sur des méthodes et savoirs non encore assimilés par l'économie, et la participation à l'économie pour assurer la pérennité de l'activité.

De cette exploration naitront :

- des statuts de coopératives ayant vocation d'être adaptés et employés par différents projets d'activités de recherche
- ainsi qu'une charte propre aux porteurs de ce projet et invitant d'autres projets à faire leurs choix propres à l'identité de leur projet et aux critères qui paveront le chemin du projet

## Premières réalisations

### Fuite en avant en skipper solitaire

Ces premières notes sont très fortement inspirées par les multiples acteurs des mondes des coopératives et de la science ouverte. [Des sources d'inspiration](inspirations.md)

[Tempête de cerveaux](https://gitlab.com/jibe-b/my-own-coop/issues) 

[Manifeste](manifest.md)

[Ou bien en écrivant un récit de science fiction ?](https://twitter.com/jibe_jeybee/status/941056398661648385)

[Premiers statuts naïfs](statuts-entreprise-coopérative.md)

## Contribution

Cette exploration prendra de l'ampleur avec les contributions de personnes de sensibilités les plus variées possibles.

Je vous invite chaleureusement à écrirer dans la [tempête de cerveaux](https://gitlab.com/jibe-b/my-own-coop/issues) toute remarque, question, suggestion ou idées incroyable :)

Et pour discuter, on peut passer par [twitter](https://twitter.com/jibe_jeybee).

Notez que tout ce qui est produit dans cette exploration est sous licence libre (CC-BY) donc les contributions que vous faites maintenant seront toujours disponibles pour vous si vous souhaitez partir dans une autre direction indépendamment.

## Concrètement

Ça progresse !

### Feuille de route

1. tempête de cerveaux

à tout moment : entremêllage de sensibilités et idées de différents individus, oscillation entre progression solitaire (condensation, gestation) et cogitation collective (expansion, confrontation, résolution)

2. écriture 
- de premiers statuts de coopérative – générateur de squelettes de status
- d'une première charte cadre pour une activité de recherche au sein du collection (open-scientist.org)

à tout moment : ce projet peut être forké (duplication du tronc et continuation dans une autre direction, de manière indépendante)

## Yeah!

